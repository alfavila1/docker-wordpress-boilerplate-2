#!/bin/bash

# Set environment variables from .env file
export $(grep -v '^#' .env | xargs)

# Change docker-compose.yaml file
CURRENT_USER=$(id -un)
CURRENT_ID=$(id -u)
sed -i "s/CURRENT_USER=.*/CURRENT_USER=$CURRENT_USER/g" .env
sed -i "s/CURRENT_ID=.*/CURRENT_ID$CURRENT_ID/g" .env

# Change nginx.conf file
sed -i "s/server_name .*/server_name $HOST;/" dev/nginx.conf

# Change docker-compose.yaml
#sed -i "s/db_db/$DB_HOST/g" docker-compose.yaml

if [ -d dev/db ]; then
  read -rp "Remove DataBase ? [y/N]: "
  if [[ "$REPLY" == [Yy]* ]]; then
      echo 'Remove DataBase ...'
      sudo rm -rf dev/db
  else
      echo 'Skip remove'
  fi
fi


if [ ! -d wp-admin ]; then
  echo "Installing Wordpress..."
  wget https://wordpress.org/latest.zip
  unzip latest.zip
  rm latest.zip
  mv wordpress/wp-config-sample.php wordpress/wp-config.php
  sed -i "s/database_name_here/$DB_DATABASE/g" wordpress/wp-config.php
  sed -i "s/password_here/$DB_PASSWORD/g" wordpress/wp-config.php
  sed -i "s/username_here/$DB_USERNAME/g" wordpress/wp-config.php
#  sed -i "s/localhost/$DB_HOST/g" wordpress/wp-config.php
  sed -i "s/localhost/db/g" wordpress/wp-config.php

  for i in $(seq 1 8)
  do
    key=$(tr -dc _A-Za-z0-9 < /dev/urandom | head -c64)
    sed -i "0,/put your unique phrase here/s/put your unique phrase here/$key/" wordpress/wp-config.php
  done
fi



# Add Certificate SSL
mkdir dev/cert
mkcert -cert-file dev/cert/ssl_certificate.pem -key-file dev/cert/ssl_certificate_key.pem "$HOST" localhost 127.0.0.1


# Add url to /etc/hosts file
if [ -n "$(grep -P "$HOST" /etc/hosts)" ]; then
  echo "Site $HOST already exists in hosts"
else
  echo "Adding in hosts:"
  echo "127.0.0.1 $HOST" | sudo tee -a /etc/hosts
fi

echo 'Change permission'
sudo chown -R "$CURRENT_USER":82 wordpress/

# Start Container
docker compose up -d

echo ""
echo "Web ready:" "https://$HOST"
echo ""
